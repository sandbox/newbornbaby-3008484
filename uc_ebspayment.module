<?php
/**
 * @file
 * Integrates ebs's redirected payment service.
 */


/**
 * Implementation of hook_menu().
 */
function uc_ebspayment_menu() {
  $items = array();
  
  $items['cart/ebspayment/complete'] = array(
    'title' => 'Order complete',
    'page callback' => 'uc_ebspayment_complete',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'uc_ebspayment.pages.inc',
  );

  return $items;
}

/**
 * Implementation of hook_ucga_display().
 */
function uc_ebspayment_ucga_display() {
  // Tell UC Google Analytics to display the e-commerce JS on the custom
  // order completion page for this module.
  if (arg(0) == 'cart' && arg(1) == 'ebspayment' && arg(2) == 'complete') {
    return TRUE;
  }
}

/**
 * Implementation of hook_form_alter().
 */
function uc_ebspayment_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'uc_cart_checkout_review_form' && ($order_id = intval($_SESSION['cart_order'])) > 0) {
    $order = uc_order_load($order_id);

    if ($order->payment_method == 'ebspayment') {
      unset($form['submit']);
      $form['#prefix'] = '<table id="two-checkout-review-table"><tr><td>';
      $form['#suffix'] = '</td><td>'. uc_ebspayment_form_html($order) .'</td></tr></table>';
    }
  }
}

/**
 * Implementation of hook_payment_method().
 *
 * @see uc_payment_method_ebspayment()
 */
function uc_ebspayment_payment_method() {
  $path = base_path() . drupal_get_path('module', 'uc_ebspayment');
  $title = variable_get('uc_ebspayment_method_title', t('Online Payment on a Secure Server:'));
  $title .= '<br /><img src="'. $path .'/logo.gif" style="position: relative; left: 2.5em;" />';

  $methods[] = array(
    'id' => 'ebspayment',
    'name' => t('Ebspayment'),
    'title' => $title,    
    'desc' => t('Redirect to Ebspayment to pay by credit card or netbanking.'),
    'callback' => 'uc_payment_method_ebspayment',
    'weight' => 3,
    'checkout' => TRUE,
    'no_gateway' => TRUE,
  );

  return $methods;
}

/**
 * Add ebspayment settings to the payment method settings form.
 *
 * @see uc_ebspayment_payment_method()
 */
function uc_payment_method_ebspayment($op, &$arg1) {
  switch ($op) {
    /**
     * Is this needed?
     */
    case 'cart-details':
      if (variable_get('uc_ebspayment_check', FALSE)) {
        if ($_SESSION['pay_method'] == 'CK') {
          $sel[2] = ' selected="selected"';
        }
        else {
          $sel[1] = ' selected="selected"';
        }
        unset($_SESSION['pay_method']);
        $details = '<div class="form-item"><b>'. t('Select your payment type:') .
          '</b> <select name="pay_method" class="form-select" id="edit-pay-method">' .
          '<option value="CC"'. $sel[1] .'>'. t('Credit card') .'</option>' .
          '<option value="CK"'. $sel[2] .'>'. t('Online check') .'</option></select></div>';
      }
      return $details;

    case 'cart-process':
      $_SESSION['pay_method'] = $_POST['pay_method'];
      return;

    case 'settings':
      $form['uc_ebspayment_accountid'] = array(
        '#type' => 'textfield',
        '#title' => t('Merchant Account Id'),
        '#description' => t('Your EBS Account Id.'),
        '#default_value' => variable_get('uc_ebspayment_accountid', ''),
        '#size' => 16,
      );
      $form['uc_ebspayment_secret_key'] = array(
        '#type' => 'textfield',
        '#title' => t('Secret Key'),
        '#description' => t('The secret key provided by EBS with your Account settings.'),
        '#default_value' => variable_get('uc_ebspayment_secret_key', 'ebskey'),
        '#size' => 16,
      );
      $form['uc_ebspayment_pageid'] = array(
        '#type' => 'textfield',
        '#title' => t('Merchant Page Id'),
        '#description' => t('Your EBS Payment Page Id. This is mandatory not setting this will make secure validation failed in EBS.'),
        '#default_value' => variable_get('uc_ebspayment_pageid', ''),
        '#size' => 16,
      );
      $form['uc_ebspayment_hash_type'] = array(
        '#type' => 'select',
        '#title' => t('EBS Hash Type'),
        '#description' => t('Select the secure hash configured with EBS payment page'),
        '#options' => array(
          'MD5' => t('MD5'),
          'SHA1' => t('SHA1'),
          'SHA512' => t('SHA512'),
        ),
        '#default_value' => variable_get('uc_ebspayment_hash_type', 'MD5'),
      );
      $form['uc_ebspayment_method_title'] = array(
        '#type' => 'textfield',
        '#title' => t('Payment method title'),
        '#description' => t('This title will be shown at the checkout page.'),
        '#default_value' => variable_get('uc_ebspayment_method_title', t('Online Payment on a Secure Server:')),
      );
      $form['uc_ebspayment_checkout_button'] = array(
        '#type' => 'textfield',
        '#title' => t('Order review submit button text'),
        '#description' => t('Provide ebspayment specific text for the submit button on the order review page.'),
        '#default_value' => variable_get('uc_ebspayment_checkout_button', t('Submit Order')),
      );
      $form['uc_ebspayment_checkout_mode'] = array(
        '#type' => 'select',
        '#title' => t('EBS checkout mode'),
        '#description' => t('Mode of Transaction. Select TEST for Testing and select LIVE when your account is made LIVE from EBS'),
        '#options' => array(
          'TEST' => t('TEST'),
          'LIVE' => t('LIVE'),
        ),
        '#default_value' => variable_get('uc_ebspayment_checkout_mode', 'TEST'),
      );
      return $form;
  }
}

/**
 * Form to build the submission to Secure.Ebs.in.
 */
function uc_ebspayment_form_html($order) {
  $country = uc_get_country_data(array('country_id' => $order->billing_country));
  /**
   * Is this required to default to India?
   */
  if ($country === FALSE) {
    $country = array(0 => array('country_iso_code_3' => 'IND'));
  }
  $deliverycountry = uc_get_country_data(array('country_id' => $order->delivery_country));
  if ($deliverycountry === FALSE) {
    $deliverycountry = array(0 => array('country_iso_code_3' => 'IND'));
  }

  $description = array();
  foreach ($order->products as $product) {
    $description[] = $product->title . ' * ' . $product->qty;
  }

  $context = array(
    'revision' => 'formatted-original',
    'type' => 'order_total',
    'subject' => array(
      'order' => $order,
    ),
  );
  $options = array(
    'sign' => FALSE,
    'dec' => '.',
    'thou' => FALSE,
  );
  $data = array(
    // Standard = 0, Direct = 1
    'channel' => '0',
    'account_id' => variable_get('uc_ebspayment_accountid', ''),
    'reference_no' => $order->order_id,
    // The amount formating is a issue, documentation not available,
    // Check with test website.
    ///**/  'amount' => uc_currency_format($order->order_total, FALSE, FALSE, '.'),
    'amount' => uc_price($order->order_total, $context, $options),
    'mode' => variable_get('uc_ebspayment_checkout_mode', 'TEST'),
    'currency' => 'INR',
    'description' => substr(implode(',', $description), 0, 255),
    // Want to use order_id or cart_get_id()?
    ///**/  'return_url' => url('cart/ebspayment/complete/', array('absolute' => TRUE)) . $order->order_id;
    'return_url' => url('cart/ebspayment/complete/'. uc_cart_get_id(),array('absolute' => TRUE)),
    // 128 charactor maximum for name
    'name' => substr($order->billing_first_name .' '. $order->billing_last_name, 0, 128),
    // 255 chars maximum for address
    'address' => _uc_ebspayment_make_address_line($order->billing_street1, $order->billing_street2, 255),
    'city' => substr($order->billing_city, 0, 32),
    'state' => uc_get_zone_code($order->billing_zone),
    'postal_code' => substr($order->billing_postal_code, 0, 10),
    'country' => $country[0]['country_iso_code_3'],
    'email' => substr($order->primary_email, 0, 100),
    'phone' => substr($order->billing_phone, 0, 20),
    // 255 chars maximum for ship_name
    'ship_name' => substr($order->delivery_first_name .' '. $order->delivery_last_name, 0, 255),
    'ship_address' => _uc_ebspayment_make_address_line($order->delivery_street1, $order->delivery_street2, 255),
    'ship_city' => substr($order->delivery_city, 0, 32),
    'ship_state' => uc_get_zone_code($order->delivery_zone),
    'ship_postal_code' => substr($order->delivery_postal_code, 0, 10),
    'ship_country' => $deliverycountry[0]['country_iso_code_3'],   
    'ship_phone' => substr($order->delivery_phone, 0, 20),
    // Docs says this is optional, but it is not.
    'page_id' => variable_get('uc_ebspayment_pageid', ''),
    // These are optional.
    /*
      'payment_option' => variable_get('uc_ebspayment_payment_option', ''),
      // Bank code provided by EBS, this will not allow payment from any other bank.
      'bank_code' => variable_get('uc_ebspayment_bank_code', ''),
    */
    // These are for direct mode transactions.
    /*
      'name_on_card' => '1 to 20 chars',
      'card_number' => '13 to 19 chars',
      'card_expiry' => 'numeric 4 digit MMYY format',
      'payment_mode' => '{1:Credit card, 2: Debit Card, 3: Net banking, 4: Cash card, 5: EMI, 6: Creditcard}',
      'card_brand' => '{1: visa, 2: Master card, 3: maestro, 4: diners club, 5: american express, 6: jcb}',
      'emi' => '[3, 6, 9, 12]',
      'card_cvv' => '3 to 4 digit cvv code',
    */
  );
  // Initialize $hashData with secret key.
  $hashData = variable_get('uc_ebspayment_secret_key', '');

  ksort($data);
  foreach ($data as $key => $value)
    if (strlen($value) > 0) $hashData .= '|' . $value;

  if ($hashData != '')
    switch(variable_get('uc_ebspayment_hash_type', 'MD5')) {
      case 'SHA512':
        $hashvalue = hash('SHA512', $hashData);
        break;
      case 'SHA1':
        $hashvalue = sha1($hashData);
        break;
      case 'MD5':
        $hashvalue = md5($hashData);
        break;
    }

  $data['secure_hash'] = strtoupper($hashvalue);
  /**
   * Check wether this is true?
   * drupal form generate additional fields, form_id, form_token and 
   * form_buildid which make secure hash validation failure in ebs website
   */

  $submit_text = variable_get('uc_ebspayment_checkout_button', t('Submit Order'));
  $form[] = '<form id="uc_ebspayment_form" method="post" action="https://secure.ebs.in/pg/ma/payment/request/"> <div>';
  foreach ($data as $name => $value) {
    $form[] = '<input type="hidden" name="' . $name . '" value="' . $value . '" />';
  }
  $form[] = '<input class="form-submit" type="button" name="op" value="' . $submit_text . '" onclick="this.form.submit();" />';
  $form[] = '</div></form>';
  return implode("\n", $form);
}

function _uc_ebspayment_make_address_line($line1, $line2, $max) {
  $len1 = strlen($line1);
  $len2 = strlen($line2);
  if ($len1 == 0 && $len2 == 0) {
    return '';
  }
  if ($len1 + $len2 < $max) {
    return $line1 . ' ' . $line2;
  }
  if ($len1 > $len2) {
    if ($len2 < $max/2) {
      return substr($line1, 0, $max - $len2 - 1) . ' ' . $line2;
    }
  }
  else if ($len2 > $len1) {
    if ($len1 < $max/2) {
      return $line1 . ' ' . substr($line2, 0, $max - $len1 - 1);
    }
  }
  return substr($line1, 0, ($max-1)/2) . ' ' . substr($line2, 0, ($max - 1)/2);
}
