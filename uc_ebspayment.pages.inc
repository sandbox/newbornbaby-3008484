<?php
/**
 * @file
 * ebspayment menu items.
 *
 */

function uc_ebspayment_complete($cart_id = 0) {
  watchdog('ebspayment', 'Receiving new order notification for order !order_id.', array('!order_id' => check_plain($_SESSION['cart_order'])));

  // process response.
  foreach (array('MerchantRefNo', 'SecureHash') as $key) {
    if (isset($_POST[$key]) && $_POST[$key]) continue;
    return t('Invalid order responce');
  }
  $hashData = variable_get('uc_ebspayment_secret_key', '');
  $hash = $_POST['SecureHash'];
  unset($_POST['SecureHash']);
  ksort($_POST);
  foreach ($_POST as $key => $value)
    if ($_POST[$key] != '') $hashData .= '|' . $_POST[$key];

  $hashValue = '';
  if ($hashData != '')
    switch(variable_get('uc_ebspayment_hash_type', 'MD5')) {
      case 'SHA512':
        $hashValue = hash('SHA512', $hashData);
        break;
      case 'SHA1':
        $hashValue = sha1($hashData);
        break;
      case 'MD5':
        $hashValue = md5($hashData);
        break;
    }
  $hashValue = strtoupper($hashValue);
  if ($hashValue != $hash) {
    watchdog('ebspayment', 'Invalid secure hash recieved');
    return t('An error has occured, Your order will be processed manually. <a href="!url">click here</a> to go back to your cart',
      array('!url' => url('cart', array('absolute' => TRUE)))
            );
  }
  $order_id = $_POST['MerchantRefNo'];
  $order = uc_order_load(intval($order_id));

  if ($order === FALSE) {
    watchdog('ebspayment', 'Invalid orderid !order_id in responce.', array('!order_id' => $order_id));
    return t('An error has occurred. Please contact us to ensure your order has submitted.');
  }

  if (intval($_SESSION['cart_order']) != $order->order_id) {
    drupal_set_message(t('Thank you for your order! EBS payment gateway will notify us once your payment has been processed.'));
    drupal_goto('cart');
  }

  $payment_status = check_plain($_POST['ResponseCode']);
  $payment_reason = check_plain($_POST['ResponseMessage']);
  $payment_amount = check_plain($_POST['Amount']);
  $payment_id	  = check_plain($_POST['PaymentID']);

  $context = array(
    'revision' => 'formatted-original',
    'type' => 'amount',
  );
  $options = array(
    'sign' => FALSE,
  );

  if($payment_status == 0){
    $comment = t('Secure EBS Payment ID: @payment_id', array('@payment_id' => $payment_id));
    uc_payment_enter($order->order_id, 'ebspayment', $payment_amount, $order->uid, $_POST, $comment);

    uc_order_comment_save($order->order_id,
      0,
      t('Payment of @amount submitted through EBS.', array('@amount' => uc_price($payment_amount, $context, $options))),
      'order',
      $payment_reason
                         );

    $_SESSION['do_complete'] = TRUE;
    drupal_goto('cart/checkout/complete');
  }
  else {
    uc_order_comment_save($order->order_id, 0, t("Responce Message: @msg", array('@msg' => $_POST['ResponseMessage'])), 'admin');
    uc_order_comment_save($order->order_id, 0, t("The customer's attempted payment from a bank account failed."), 'admin');
    $order->order_status = 'Canceled';

    drupal_set_message(t('Your EBS payment was cancelled. Please feel free to continue shopping or contact us for assistance.'));
    uc_order_save($order);
    drupal_goto('cart');
  }
}
